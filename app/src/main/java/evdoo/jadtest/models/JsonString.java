package evdoo.jadtest.models;

public class JsonString {

    private String key;
    private String value;

    public JsonString(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
