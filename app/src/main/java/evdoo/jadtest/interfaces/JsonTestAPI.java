package evdoo.jadtest.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JsonTestAPI {

    @GET("http://ip.jsontest.com/")
    Call<String> getIp();

    @GET("http://date.jsontest.com/")
    Call<String> getDateAndTime();

    @GET("http://headers.jsontest.com/")
    Call<String> getHeaders();

    @GET("http://echo.jsontest.com/{value}/")
    Call<String> getEcho(@Path(value = "value", encoded = true) String value);

    @GET("http://validate.jsontest.com/")
    Call<String> getValidation(@Query("json") String json);
}