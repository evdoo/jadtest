package evdoo.jadtest;

import android.app.Application;
import android.util.Log;
import evdoo.jadtest.interfaces.JsonTestAPI;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.content.ContentValues.TAG;

public class App extends Application {

    private static JsonTestAPI jsonTestApi;
    private static Retrofit retrofit;
    private static String baseUrl;

    @Override
    public void onCreate() {
        super.onCreate();
        baseUrl = getResources().getString(R.string.base_url);
        init();
    }

    public static void init() {
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .addInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                            @Override
                            public void log(String message) {
                                Log.d(TAG, message);
                            }
                        }).setLevel(HttpLoggingInterceptor.Level.BODY))
                        .build())
                .build();
    }

    public static JsonTestAPI getApi() {
        jsonTestApi = retrofit.create(JsonTestAPI.class);
        return jsonTestApi;
    }
}