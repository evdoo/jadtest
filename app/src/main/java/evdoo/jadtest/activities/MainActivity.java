package evdoo.jadtest.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import butterknife.Bind;
import butterknife.ButterKnife;
import evdoo.jadtest.R;
import evdoo.jadtest.adapters.ViewPagerAdapter;
import evdoo.jadtest.fragments.AboutMeFragment;
import evdoo.jadtest.fragments.MainCardsFragment;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.main_view_pager)
    ViewPager mainViewPager;

    @Bind(R.id.navigation)
    BottomNavigationView navigation;

    private MainCardsFragment mainCardsFragment;
    private AboutMeFragment aboutMeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mainCardsFragment = new MainCardsFragment();
        aboutMeFragment = new AboutMeFragment();

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mainCardsFragment, getResources().getString(R.string.cards));
        adapter.addFragment(aboutMeFragment, getResources().getString(R.string.about));
        mainViewPager.setAdapter(adapter);

        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                navigation.getMenu().getItem(position).setChecked(true);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        navigation.getMenu().getItem(0).setChecked(true);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.cards:
                        mainViewPager.setCurrentItem(0, true);
                        break;
                    case R.id.about:
                        mainViewPager.setCurrentItem(1, true);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_HOME) {
            finish();
        }
        return true;
    }
}