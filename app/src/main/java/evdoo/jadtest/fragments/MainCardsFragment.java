package evdoo.jadtest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import butterknife.ButterKnife;
import evdoo.jadtest.App;
import evdoo.jadtest.R;
import evdoo.jadtest.models.JsonString;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

public class MainCardsFragment extends Fragment {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.ip_address_card_view)
    CardView ipAddressCardView;

    @Bind(R.id.ip_list)
    LinearLayout ipList;

    @Bind(R.id.date_and_time_card_view)
    CardView dateAndTimeCardView;

    @Bind(R.id.date_and_time_list)
    LinearLayout dateAndTimeList;

    @Bind(R.id.echo_json_card_view)
    CardView echoJsonCardView;

    @Bind(R.id.echo_edit_text)
    EditText echoEditText;

    @Bind(R.id.echo_submit_button)
    Button echoSubmitButton;

    @Bind(R.id.echo_list)
    LinearLayout echoList;

    @Bind(R.id.validation_card_view)
    CardView validationCardView;

    @Bind(R.id.validation_edi_text)
    EditText validationEditText;

    @Bind(R.id.validation_submit_button)
    Button validationSubmitButton;

    @Bind(R.id.validation_list)
    LinearLayout validationList;

    @Bind(R.id.headers_card_view)
    CardView headersCardView;

    @Bind(R.id.headers_list)
    LinearLayout headersList;

    CardCallback callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_card, container, false);
        ButterKnife.bind(this, view);

        toolbar.setTitle(getResources().getString(R.string.app_name));

        echoSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = echoEditText.getText().toString();
                if (s.equals("")) {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_input), Toast.LENGTH_SHORT).show();
                } else {
                    String[] items = s.split(",");
                    String value = "";
                    for (int i = 0; i < items.length; i++) {
                        value += URLEncoder.encode(items[i].trim()) + "/";
                    }
                        callback = new CardCallback(echoList.getId());
                        App.getApi().getEcho(value).enqueue(callback);
                }
            }
        });

        validationSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = validationEditText.getText().toString();
                if (s.equals("")) {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_input), Toast.LENGTH_SHORT).show();
                } else {
                    callback = new CardCallback(validationList.getId());
                    App.getApi().getValidation(s).enqueue(callback);
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        callback = new CardCallback(ipList.getId());
        App.getApi().getIp().enqueue(callback);

        callback = new CardCallback(dateAndTimeList.getId());
        App.getApi().getDateAndTime().enqueue(callback);

        callback = new CardCallback(headersList.getId());
        App.getApi().getHeaders().enqueue(callback);
    }

    private class CardCallback implements Callback<String> {

        private int listId;

        CardCallback(int listId) {
            this.listId = listId;
        }

        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            if (response.isSuccessful() && response.body() != null) {
                String stringBody = response.body();
                String string = URLDecoder.decode(stringBody);
                try {
                    JSONObject object = new JSONObject(string);
                    Iterator<String> iterator = object.keys();
                    ArrayList<JsonString> strings = new ArrayList<>();
                    while(iterator.hasNext()) {
                        String key = iterator.next();
                        String value = String.valueOf(object.get(key));
                        JsonString s = new JsonString(key, value);
                        strings.add(s);
                    }

                    LinearLayout layout = (LinearLayout) getView().findViewById(listId);
                    layout.setVisibility(View.VISIBLE);
                    layout.removeAllViews();

                    for (JsonString s : strings) {
                        LinearLayout item = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.item_card,
                                layout, false);
                        TextView key = (TextView) item.findViewById(R.id.key_text_view);
                        TextView value = (TextView) item.findViewById(R.id.value_text_view);

                        key.setText(s.getKey());
                        value.setText(s.getValue());
                        layout.addView(item);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), getResources().getString(R.string.smth_wrong),
                            Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            t.printStackTrace();
            Toast.makeText(getContext(), getResources().getString(R.string.smth_wrong),
                    Toast.LENGTH_SHORT).show();
        }
    }
}