package evdoo.jadtest.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import evdoo.jadtest.R;

public class AboutMeFragment extends Fragment {

    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.contacts_card_view)
    CardView contactsCardView;

    @Bind(R.id.email_image_button)
    ImageButton emailImageButton;

    @Bind(R.id.phone_image_button)
    ImageButton phoneImageButton;

    @Bind(R.id.sms_image_button)
    ImageButton smsImageButton;

    @Bind(R.id.telegram_image_button)
    ImageButton telegramImageButton;

    @Bind(R.id.open_on_market_button)
    Button openOnMarketButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_me, container, false);
        ButterKnife.bind(this, view);

        toolbar.setTitle(getResources().getString(R.string.name));
        collapsingToolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapsingToolbar.setExpandedTitleColor(Color.WHITE);

        emailImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = getContext().getResources().getString(R.string.email);

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL , new String[] {email});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivity(Intent.createChooser(i, getResources().getString(R.string.choose_app)));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_such_app), Toast.LENGTH_SHORT).show();
                }
            }
        });

        phoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = getContext().getResources().getString(R.string.phone);
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + tel));
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_such_app), Toast.LENGTH_SHORT).show();
                }
            }
        });

        smsImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = getContext().getResources().getString(R.string.phone);

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setType("message/rfc822");
                intent.setData(Uri.parse("sms:" + tel));
                try {
                    startActivity(Intent.createChooser(intent, getResources().getString(R.string.choose_app)));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_such_app), Toast.LENGTH_SHORT).show();
                }
            }
        });

        telegramImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String telegramLink = "https://t.me/evdoo";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(telegramLink));
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(Intent.createChooser(intent, getResources().getString(R.string.choose_app)));
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_such_app), Toast.LENGTH_SHORT).show();
                }
            }
        });

        openOnMarketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=evdoo.coloring_book"));
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(Intent.createChooser(intent, getResources().getString(R.string.open_on_market)));
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_such_app), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }
}